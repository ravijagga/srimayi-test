const UsersModel = require('./users.model');
const { commonErrors } = require('../error');

module.exports.getUserById = async function (id) {
  const result = await UsersModel.findById(id).select('-emailToken -hash').exec();
  if (!result) throw new commonErrors.NotFoundError();
  return result;
};
