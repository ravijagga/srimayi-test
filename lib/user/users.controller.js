const usersService = require('./users.service');

module.exports.getLoggedInUser = async function (req, res) {
  const user = await usersService.getUserById(req.user._id);

  res.send(user);
}
