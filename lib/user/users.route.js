const express = require('express');
const router = express.Router();
const usersController = require('./users.controller');
const { userAuth } = require('../auth/auth.middleware');

// Our Routes
router.route('/me')
  .get(userAuth, usersController.getLoggedInUser);

module.exports = router;
