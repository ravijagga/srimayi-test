import { Component, OnInit } from '@angular/core';
import { MyAuthService } from '../auth/my-auth.service';
import questionsData from './questions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private objectKeys = Object.keys;
  private currSubject = 'physics';
  private questions;

  constructor(private myAuthService: MyAuthService, private router: Router) { }

  ngOnInit() {
    this.loadQuests(this.currSubject);
    console.log(this.questions);
  }

  loadQuests(subject) {
    this.questions = questionsData[subject];
  }

  changeSubject(subject) {
    this.currSubject = subject;
    this.loadQuests(this.currSubject);
    console.log(this.questions);
  }

  logOut() {
    this.myAuthService.logOut();
    this.router.navigate(['login']);
  }

}
