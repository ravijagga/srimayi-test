import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import config from '../../config';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class MyAuthService {

  constructor(private http: HttpClient) { }

  baseUrl = config.baseUrl;

  isLoggedIn() {
    return !!localStorage.getItem('authToken');
  }

  logOut() {
    localStorage.removeItem('authToken');
  }

  getAuthToken(email, password) {
    return this.http.post(
      `${this.baseUrl}/auth`,
      { email, password },
      { observe: 'response' }
    );
  }
}
