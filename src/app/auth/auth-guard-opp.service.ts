import { Injectable } from '@angular/core';
import { MyAuthService } from './my-auth.service';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardOppService {

  constructor(private myAuthService: MyAuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return !this.myAuthService.isLoggedIn();
  }
}
