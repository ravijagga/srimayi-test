import { Component } from '@angular/core';
import { MyAuthService } from './../my-auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(
    private myAuthService: MyAuthService,
    private router: Router
  ) { }

  login(user) {
    console.log(user.value);

    // Set auth token in localStorage
    this.myAuthService
      .getAuthToken(user.value.email, user.value.password)
      .subscribe(res => {
        if (res.ok) {
          // Set Token in Local Storage
          localStorage.setItem('authToken', res.headers.get('x-auth-token'));

          // Navigate to dashboard
          this.router.navigate(['dashboard']);
        }
      });

  }
}
