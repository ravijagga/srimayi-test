const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jwt = require('jsonwebtoken');
const envConfig = require('../../config/env');
const bcrypt = require('bcrypt');

const userSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
    default: new mongoose.Types.ObjectId()
  },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, index: true },
  hash: { type: String, default: null },
}, { versionKey: false, timestamps: true });

userSchema.statics.generateObjectId = function () {
  const objectId = new mongoose.Types.ObjectId();
  return objectId;
};

userSchema.statics.getAuthToken = function (user) {
  const authToken = jwt.sign(user, envConfig.JWT_SECRET_KEY);
  return authToken;
};

userSchema.statics.verifyAuthTokenAndGetUser = function (authToken) {
  const user = jwt.verify(authToken, envConfig.JWT_SECRET_KEY);
  return user;
};

userSchema.statics.getPasswordHash = async function (password) {
  const saltRounds = 10;
  const salt = await bcrypt.genSalt(saltRounds);
  const hash = await bcrypt.hash(password, salt);
  return hash;
};

module.exports = mongoose.model('User', userSchema, 'users');
