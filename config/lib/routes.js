const express = require('express');
const path = require('path');

const usersRoute = require('../../lib/user/users.route');
const authRoute = require('../../lib/auth/auth.route');

module.exports = function (app) {
  // API Components
  app.use('/api/users', usersRoute);
  app.use('/api/auth', authRoute);

  // Catch-All
  app.use('*', (req, res) => {
    res.sendFile(path.join(__basedir, 'dist', 'srimayi-test', 'index.html'));
  });
};
