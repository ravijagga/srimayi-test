const questions = {
  physics: {
    q1: 'physics1',
    q2: 'physics2'
  },
  chemistry: {
    q1: 'chem1',
    q2: 'chem2'
  },
  maths: {
    q1: 'math1',
    q2: 'math2'
  }
};

export default questions;
