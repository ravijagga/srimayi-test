module.exports = {
  env: "development",
  app: {
    title: "Srimayi Test"
  },
  server: {
    port: 3000
  },
  modules: {
    users: {
      perPageLimit: 5
    }
  },
  JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,

};
